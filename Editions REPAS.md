---
title: Qui sont les Éditions REPAS ?
header:
  image: Images/IMG_Ardeche.jpg
permalink: /les-editions/
htmlClass: taille-étroite
---


# Les éditions REPAS

[[sommaire]]

## L'association

L'association REPAS (loi 1901) est une structure d'édition indépendante et d'éducation populaire. Historiquement installée dans la Drôme, à Valence, elle a pour objet de **faire découvrir, documenter et publier des histoires de projets inspirants de l'économie sociale et solidaire**. Elle est convaincue que les réponses aux multiples crises sociales et écologiques en cours sont déjà en germe dans les alternatives collectives, bien vivantes dans tous les territoires et dans de nombreux secteurs.

Créées en 2003 par des collectifs membres du [réseau REPAS](/reseau-repas/) pour publier leurs témoignages, les éditions REPAS sont aujourd'hui fières de compter 23 publications, 9 rééditions de titres et près de 80 de [lieux de vente permanents](/points-de-vente/) en France. L'essentiel des titres est rassemblé dans la collection "Pratiques Utopiques" pour témoigner d'alternatives sociales, créatives et soutenables, en pratique aux quatre coins de la France.

L'association est animée au quotidien par une salariée polyvalente, à Valence, et sous la coordination du comité de pilotage bénévole. Si nous ne pouvons pas encore élargir l'équipe sur la durée, nous sommes ouverts aux propositions de stages courts afin de faire découvrir le monde du livre et l'économie sociale et solidaire à de nouvelles personnes.

## Notre projet éditorial
**Les Editions REPAS publient des récits et témoignages d'expériences alternatives, collectives et solidaires. Nos livres souhaitent montrer qu'il y a toujours place, ici et maintenant comme hier et ailleurs, pour des réalisations qui s'inscrivent dans le concret de pratiques libres et solidaires.**

Face au morcellement du travail, à la désertification des campagnes, à la déshumanisation dans les cités, à l’exclusion et aux enjeux posés par la crise écologique, des entreprises, des groupes, des associations ou des individus apportent des réponses originales et adaptées à ces questions de société qui paraissent parfois insolubles.

Concrètement il s’agit de bâtir cet « autre monde possible » qui ne peut objectivement se décliner qu’au pluriel. Exemples de démocratie économique, d’initiative citoyenne ou d’innovation sociale, elles bousculent également quelques sacro-saints principes de notre société marchande, démontrant au quotidien que l’association est plus enrichissante que la compétition, que la coopération vaut mieux que la concurrence ou que l’autogestion permet de reprendre le pouvoir sur sa vie. Ces *Pratiques utopiques* espèrent, par ce biais, encourager celles et ceux qui sont insatisfaits du monde dans lequel ils vivent, à faire le pas vers d’autres possibles. L’utopie est à portée de main.

## Pratiquer l'édition engagée

La filière du livre n'est bien sûr pas exempte de dynamiques environnementales, sociales et hiérarchiques toxiques pour le monde actuel et à venir. Les éditions REPAS tâchent d'ancrer leurs pratiques au plus près des valeurs "utopiques" qui guident notre ligne éditoriale et l'action de nos auteurs et autrices.

### Une association d'auteurs-éditeurs
Les éditions REPAS sont nées du constat qu'il est très difficile, pour une autrice ou un auteur, d'exister dans le très concurrentiel marché du livre sans devoir céder légalement ses écrits à une maison d'édition, ou se trouver isolé et sans appui avec un livre en auto-édition. 

Parce que nos livres sont des histoires collectives qui ne se construisent qu'à plusieurs, que nos histoires évoluent, grandissent, et que les autrices et auteurs ne sont que les plumes motivées et bienveillantes de nos aventures... nous avons mis sur pied un **modèle d'édition qui implique les auteurs** et porteuses des histoires dans la conception de l'identité du livre (le titre, les visuels), garantit aux plumes de conserver leurs droits d'auteurs sur les textes et de pouvoir les réemployer à leur guise plus tard, et qui co-construit le modèle économique de chaque ouvrage avec l'ensemble des personnes portant l'envie de faire ce livre. Un fonctionnement en **démocratie économique et créative** pour garantir la qualité des ouvrages et le pouvoir d'agir des parties prenantes.

### Une attention à l'écologie du livre
Nous empruntons l'expression à l'[Association pour l'écologie du livre](https://ecologiedulivre.org/). Notre enjeu ici est de se positionner à contre-courant des dynamiques productivistes et démentielles du secteur de l'édition en France, en misant sur des **ouvrages à longue durée de vie**, sur des **rythmes de publication respectueux des auteurs** et sur des **tirages limités** favorisant la réédition. Ces choix nous permettent de ne jamais envoyer d'ouvrages au pilon, contrairement à 70% de la production littéraire annuelle française.

### Créer et partager des biens communs

Nous pensons les livres des éditions REPAS comme des [biens communs](https://fr.wikipedia.org/wiki/Communs) dont l'usage, non discriminatoire et le plus largement partagé, doit prévaloir sur les enjeux de propriété privée et de rentabilité financière.
Les textes, dont la propriété intellectuelle inaliénable revient aux auteurs et autrices, sont ainsi confiés aux éditions REPAS pour les faire vivre le plus largement et longtemps possibles, auprès de publics variés et aux quatre coins de la France.

Pour accompagner cette diffusion matérielle, nous avons fait le choix de la **publication complète des textes** des éditions REPAS, en libre accès sur notre site internet sous forme de [LYBER](http://www.lyber-eclat.net/lybertxt/). Sur l'impulsion des Editions de l'Eclat et des Editions du Commun, nous publions tous nos textes en ligne depuis 2021 et militons ainsi pour la diffusion non-commerciale des communs de la connaissance.
