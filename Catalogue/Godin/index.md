---
Edition: Editions REPAS
DépotLégal: 2008, 2022
Auteur: Jean-François Draperi
Titre: Godin, inventeur de l'économie sociale
SousTitre: Une histoire du Familistère de Guise
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-19-8
Pages: 193 pages
Prix: 17
Etat: Disponible
Résumé: |
    Fondé par Jean-Baptiste André Godin (1817-1888), le [familistère de Guise](https://www.familistere.com/fr) (1870-1968) apparaît aujourd'hui comme un des modèles les plus aboutis d'une alternative à l'entreprise capitaliste. L'objet de ce livre est de montrer qu'à travers cette formidable aventure, Godin prouve qu'il est possible de permettre à chacun de bien vivre, dans un habitat confortable et par un travail digne, où il est respecté, sans passer par la violence et sans appauvrir quiconque.

    En concevant cette coopérative d'habitat, de production et de consommation et cet ensemble de mutuelles et d'associations qu'est le familistère, Godin s'inscrit en rupture aussi bien avec le père de l'organisation scientifique du travail, F.W. Taylor, qu'avec la critique du capitalisme formulée par K. Marx. Ce livre démontre qu'on peut considérer Godin comme l'un des fondateurs de l'économie sociale et sans doute le plus moderne d'entre eux.

    Godin constitue en effet le principal chaînon entre le premier XIXe siècle, celui de Fourier et des utopies socialistes, et le second XIXe siècle, celui de Marx, de Taylor et de la grande industrie. Son œuvre est irréductible à la fois à la pensée libérale et fonctionnelle, et à la pensée marxiste. Elle est l'une de celles qui contribuent à l'émergence d'une pensée et d'une pratique d'économie sociale. Au sein de cette tradition, Godin occupe une place à part. Ses propositions ne concernent pas seulement les convaincus - les militants recherchant une alternative -, elles s'adressent à tous les Hommes.
Tags:
- Economie sociale et solidaire
- Coopérative
- Collectif
SiteWeb: https://www.familistere.com/fr
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/godin-inventeur-de-l-economie-sociale-jean-francois-draperi?_ga=2.210501397.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272006-godin-inventeur-de-l-economie-sociale-mutualiser-cooperer-s-associer-3e-edition-jean-francois-draperi/?utm_source=dmg
Couverture:
Bandeau: 3ème réédition
AccueilOrdre: 5
Adresse: Cité Familistere, 02120 Guise
Latitude: 49.904456
Longitude: 3.62508
Couleurs:
  PageTitres: '#20749a'
  Titre: '#f5f1ec'
  Texte: '#f5f1ec'
---

## L'auteur

**Jean-François Draperi** est l'ancien directeur du Centre d'économie sociale du Conservatoire des Arts et métiers (Ceste-Cnam) à Paris et ex-rédacteur en chef de la RECMA (Revue internationale de l'économie sociale). A côté de son activité de chercheur et d'enseignant, il intervient dans de nombreuses manifestations liées à l'économie sociale et solidaire. Il est entre autres l'auteur de « Rendre possible un autre monde » (Presses de l'économie sociale, 2005) et de « L'économie sociale. Utopies, pratiques, principes » (Presses de l'économie sociale, 2005).


## L'histoire

Fondé par Jean-Baptiste André Godin (1817-1888), le familistère de Guise (1870-1968) apparaît aujourd'hui comme un des modèles les plus aboutis d'une alternative à l'entreprise capitaliste. L'objet de ce livre est de montrer qu'à travers cette formidable aventure, Godin prouve qu'il est possible de permettre à chacun de bien vivre, dans un habitat confortable et par un travail digne, où il est respecté, sans passer par la violence et sans appauvrir quiconque. En concevant cette coopérative d'habitat, de production et de consommation et cet ensemble de mutuelles et d'associations qu'est le familistère, Godin s'inscrit en rupture aussi bien avec le père de l'organisation scientifique du travail, F.W. Taylor, qu'avec la critique du capitalisme formulée par K. Marx. Ce livre démontre qu'on peut considérer Godin comme l'un des fondateurs de l'économie sociale et sans doute le plus moderne d'entre eux.

Godin constitue en effet le principal chaînon entre le premier XIXe siècle, celui de Fourier et des utopies socialistes, et le second XIXe siècle, celui de Marx, de Taylor et de la grande industrie. Son œuvre est irréductible à la fois à la pensée libérale et fonctionnelle, et à la pensée marxiste. Elle est l'une de celles qui contribuent à l'émergence d'une pensée et d'une pratique d'économie sociale. Au sein de cette tradition, Godin occupe une place à part. Ses propositions ne concernent pas seulement les convaincus - les militants recherchant une alternative -, elles s'adressent à tous les hommes.


## Extrait

> Cette histoire s'écrit en un siècle. Un siècle au cours duquel toutes les grandes entreprises d'économie sociale ont connu des transformations profondes ou ont disparu, comme les autres formes d'entreprise. La durée du Familistère nécessite une explication originale en raison de la spécificité de son organisation : association intégratrice, marginale dans ses fonctionnements, le Familistère est un très rare cas d'organisation alternative durable. Si en fin de compte l'association du Familistère s'est maintenue si longtemps sans trop se banaliser, c'est-à-dire en gardant l'essentiel de ses caractères spécifiques, c'est par la vertu de l'articulation de deux facteurs : le charisme du fondateur et la création de règles juridiques fortes.
> La création d'une organisation d'économie sociale est fréquemment le lieu de confrontation entre une légitimité charismatique et une légitimité associative ou coopérative d'inspiration égalitaire. Le Familistère Godin n'était pas un lieu d'expression égalitaire pour tous ses membres. Il existait une hiérarchie liée au mérite et à la tête un administrateur gérant disposant de forts pouvoirs… et héritant de Godin.
> L'administrateur poursuit l'oeuvre et le respect des familistériens pour le fondateur et son représentant était encore très fort un siècle après la fondation.
> -- page 63

## Le commentaire des éditeurs

Il peut paraître étrange de trouver dans une collection qui rassemble des témoignages directs sur des « pratiques utopiques » contemporaines, un ouvrage comme celui-ci. Que peut nous apprendre en effet une histoire née au XIXe siècle en pleine révolution industrielle, dans un contexte politique, économique, social et culturel a priori si différent du nôtre ? Quel intérêt, autre qu'historique, peut-il apporter à une réflexion sur l'alternative aujourd'hui ? En publiant ce livre, les éditions REPAS prennent le pari que le lecteur du XXIe siècle y trouvera plus d'un écho à ses propres réflexions et pratiques. Godin est un personnage hors du commun, une sorte de génie précurseur qui anticipe sur bien des points, mais c'est avant tout un praticien, c'est-à-dire un individu qui crée, agit, construit.

La classe de Madame Lobjeois à l'école du Familistère de GuiseIl prouve, en ce XIXe siècle industriel, que les chemins du travail et de la production peuvent dès cette époque être balisés autrement que selon les normes qui s'imposent alors et se perfectionneront ensuite avec Taylor, au sein du modèle de la grande entreprise de production de masse. Il démontre que l'alternative est de tous les temps et qu'on peut construire l'histoire par les actes d'abord et pas seulement par la pensée. Il s'affirme expérimentateur, croit en la possibilité des individus de changer leur environnement, de le maîtriser et de se l'approprier. Il ne fige pas l'individu dans une situation bloquée mais mise sur sa capacité à progresser grâce à l'éducation qui seule lui permettra d'accéder au statut de coopérateur. Il privilégie l'association et n'hésite pas à mettre ses idées en pratique avec les hommes (et les femmes - autre anticipation de Godin et non des moindres) avec lesquels il bâtit, bien au-delà du travail, une véritable contre-société coopérative. Pour toutes ces raisons, nous pouvons nous sentir aujourd'hui ses héritiers.

« Les idées, disait-il en 1884, ne reçoivent pas tout d'un coup leur application intégrale ; elles font leur chemin peu à peu et ce n'est qu'en les soumettant à l'examen et à la discussion qu'on ouvre la voie pour l'avenir. » C'est bien là la mission de ce livre.

