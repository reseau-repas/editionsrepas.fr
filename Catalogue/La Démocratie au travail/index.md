---
Edition: Editions REPAS
DépotLégal: 2023
Auteur: Roger Daviau
Titre: La Démocratie au travail
SousTitre: La SAPO, Société anonyme à participation ouvrière
Préface: Préface de Jean-François Draperi
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-20-4
Pages: 198 pages
Prix: 17
Etat: Disponible
Résumé: |
    Quel est cet ovni juridique, né en 1917, qu'on appelle la Sapo, la société anonyme à participation ouvrière ? Ce statut méconnu, peu utilisé pendant plus d'un siècle, présente pourtant une modernité et un intérêt qui peuvent aujourd'hui le rendre attractif et pertinent pour une économie plus juste et équitable. De quoi lui donner une nouvelle jeunesse !

    Roger Daviau, accompagnateur à la création et reprise d'entreprises pendant près de 24 ans et fondateur-gérant de deux coopératives, s'est passionné pour ce statut qui vise à unir capital et travail en créant, à côté des actions de capital, « des actions de travail ». Celles-ci permettent aux salariés d’accéder aux organes de direction des entreprises et de bénéficier de la distribution de dividendes. Une utopie ? Non, une réalité qu'ont expérimenté de trop rares entreprises comme les quatre que nous fait rencontrer l'auteur dans ce livre. Ce dernier est aussi une introduction historique, juridique et économique à la Sapo. Ce faisant, il souhaite faire découvrir cette formule juridique et inciter les créateurs d'entreprise soucieux de participation, les entrepreneurs qui veulent impliquer davantage leurs salariés à leurs côtés ou les porteurs de projets de reprise d’entreprise par les salariés, à l'utiliser davantage.
Tags:
- Economie sociale et solidaire
- Démocratie
- Travail
SiteWeb: https://www.helloasso.com/associations/association-de-promotion-des-societes-a-participation-ouvriere-ap-sapo
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/la-democratie-au-travail-roger-daviau
TrouverLibrairie:
Couverture:
Bandeau: Nouveauté
AccueilOrdre: 2
Couleurs:
  Fond: '#e76f3b'
  Titre: '#ffef5e'
  Texte: '#ffef5e'
  PageTitres: '#eb841d'
---

## L'auteur

Après un début de carrière technique, **Roger Daviau** est passé au milieu des années 1990 dans le domaine de la gestion, et particulièrement de l’accompagnement à la création/reprise d’entreprise.
Devenu fondateur et gérant de 2 Coopératives, dont une Coop d’Activités et d’Emploi (CAE), il a découvert, à la suite d'une formation au CNAM-CESTES, l’existence de la SAPO – Sté Anonyme à Participation Ouvrière. Passionné par les modèles coopératifs, ses lectures multiples sur le sujet se sont orientées, depuis la fin des années 2000, vers tout ce qui touche à cette formule juridique inconnue.


## Extrait

> Cette formule juridique est, à mon avis, apparue bien trop en avance. Si elle a pu être mal reçue – voire dissimulée – par certains acteurs au début du XXe siècle dans un contexte qui s’y prêtait peu, il se pourrait que les conditions économiques et sociales du début du XXIe siècle soient bien plus en phase avec ses évolutions sociales. La Sapo constitue encore une sorte de nouveauté au regard du peu de mise en oeuvre qu’elle a connu. Loin de moi l’idée d’avoir découvert une « pépite » et de vouloir en faire une formule passe-partout, encore moins une solution absolue pour développer les entreprises de notre époque. Mais je considère que, maintenant plus que jamais, elle peut fournir des réponses à des configurations humaines et économiques que j’ai pu rencontrer et sans doute à bien d’autres.
> -- page 25

## Le mot de l'éditeur

*	Une **présentation claire, accessible** et très complète d’un statut d’entreprise méconnu, susceptible de répondre à nombre de questionnements sur la place de la démocratie dans le monde du travail. 
*	Un **panorama historique** et politique passionnant retraçant le parcours de la Sapo et des idées participationnistes en France.
*	Des témoignages actuels de dirigeants et salariés au sein de Sapo, pour donner à voir les **applications concrètes** de ce statut dans divers secteurs d’activités.
