---
Edition: Editions REPAS
DépotLégal: 2014
Réédition: 2021
Autrice: Béatrice Barras
Titre: Moutons Rebelles
SousTitre: Ardelaine, la fibre développement local
Préface: Préface de Jean-François Draperi
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-06-8
Pages: 248 pages
Prix: 17
Etat: Disponible
Résumé: |
    En 1975, cinq amis, sans un sou en poche, décident de redonner vie à la dernière filature d’Ardèche tombée en ruines. Ils font aussi le pari de recréer la filière laine de leur région, pari qu’ils tiendront par la force de l’équipe et de la coopération qui demeurera le moteur essentiel de leur histoire, racontée ici. Mais au-delà de leur témoignage, ce livre montre comment chacun, même dans les situations les plus improbables et surtout s’il ne le fait pas seul, peut reprendre du pouvoir sur sa vie.

    C'est donc bien plus que l'histoire d'une entreprise qui est rapportée dans cet ouvrage, celle de la [Scop Ardelaine](https://www.ardelaine.fr/) : c'est l'aventure humaine qui, d'une situation improbable, mène à la réussite d'un projet économique, social et local.
Tags:
- Coopérative
- Développement rural
- Travail
- Collectif
SiteWeb: https://www.ardelaine.fr/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/moutons-rebelles-beatrice-barras
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272068-moutons-rebelles-ardelaine-la-fibre-developpement-local-2e-edition-beatrice-barras/
Couverture:
Couleurs:
  Fond: '#00485e'
  PageTitres: '#095a6e'
  Titre: '#fef4da'
  Texte: '#fef4da'
Bandeau: 3ème réédition
AccueilOrdre: 6
Adresse: Quartier Puausson, 07190 Saint-Pierreville
Latitude: 44.809803
Longitude: 4.513119
Vidéos:
  https://www.youtube.com/watch?v=IjosHqba6iM: Ardelaine – Travailler la laine autrement
  https://www.youtube.com/watch?v=_aGiVMOoYEw: Ardelaine racontée par Béatrice Barras
  https://www.youtube.com/watch?v=LJrvXVAFOXM: Cécile Perradin, membre de la coopérative Ardelaine
  https://www.youtube.com/watch?v=bShVU4fZ2d4: "Ardelaine, d'un fil à l'autre : histoire d'une anomalie économique"
  https://www.youtube.com/watch?v=zoapTVuJEIg: Béatrice Barras, membre de la coopérative Ardelaine
---


## L'autrice

**Béatrice Barras** est l'une des fondatrices de la Scop Ardelaine dans laquelle elle travaille toujours. Elle a également assumé diverses responsabilités auprès de différentes structures de l'économie sociale et solidaire, en particulier lorsqu'elle fut présidente du comité d'éthique de la NEF. Elle a également publié aux éditions REPAS : « Chantier ouvert au public. Le Viel Audon, village coopératif » (2014) et « Une Cité aux mains fertiles » (2019), et a participé à l'ouvrage « Quand l'entreprise apprend à vivre » (éditions Charles Léopold Mayer, 2002) consacré au compagnonnage alternatif et solidaire du réseau REPAS.


## Extraits

>  Le 12 octobre 1972, Gérard et moi roulions vers St-Pierreville avec une amie. La 2 CV peinait. Après une côte raide, une descente impressionnante dans une vallée aux pentes abruptes et terriblement déserte, de tournant en tournant, nous arrivons enfin dans un petit village ardéchois des « Boutières ». (...)
>
> Nous nous avançons prudemment. Nous pénétrons dans la pièce des cardes. C’est magique, tout est en place, chaque carde semble prête à redémarrer, les bobines pleines de laine, comme si tout s’était figé « pour cent ans » : toiles d’araignées, gravats, fuites d’eau et planchers troués en font le décor. L’étage supérieur est impénétrable : les poutres s’enchevêtrent au milieu d’une tonne de tuiles cassées.
> -- pages 13-15


> Après avoir découvert la filature de St-Pierreville, ou ce qu’il en restait, nous étions préoccupés : si rien n’est fait, tout va s’écrouler ; il faudrait mobiliser des gens, créer une association. Ces machines sont un patrimoine unique dans le département ; on pourrait faire un musée ; qui peut s’y intéresser ? Nous y réfléchissons, nous en parlons souvent, puis un jour, nous n’y tenons plus : nous décrochons le téléphone et demandons à l’opératrice de Privas de nous passer le 16 à St-Pierreville :
> - Bonjour madame, vous vous souvenez, nous étions venus il y a trois semaines environ ?
> - Oui, je vois…
> - Écoutez, nous ne savons pas encore comment, mais nous allons essayer de faire quelque chose pour la filature.
> - Vous êtes la providence, nous répondit-elle, la gorge nouée.
>
> -- pages 20-21

## Le commentaire des éditeurs

Il ne faut pas croire que la réussite économique d'une entreprise tient aux critères techniques qu'on a l'habitude de voir mettre en avant : le capital, la formation technique, l'étude du marché, la spécialisation… L'histoire d'Ardelaine aurait même tendance à démontrer le contraire ! Voilà une bande d'amis qui crée leur entreprise avec seulement 2000 francs de capital (oui, vous avez bien lu : 2000 francs !) et qui n'y connaissent alors quasiment rien ni à l'industrie de la laine ni à la gestion d'une entreprise.

Il y a là un architecte, une orthophoniste, un maçon, une étudiante en rupture de ban, un technicien agricole. La petite équipe a entre 20 et 30 ans et surfe sur l'utopie de l'après 1968, la tête dans les étoiles mais les pieds bien fichés sur terre. La reprise de la dernière filature du département, un coup de cœur, un coup de main, est aussi un cou tordu aux discours lénifiants d'alors sur l'impossible renaissance d'un monde rural voué à la désertification. Mais si le pari a pu être tenu, c'est que la perséverance, la solidarité et l'ingéniosité collective ont été fortement sollicités.

Jean François Draperi, le préfacier du livre, le dit fort bien :

>  A travers le prisme coopératif qu'ils proposent, les associés d'Ardelaine nous invitent à revisiter l'ensemble des enjeux sociétaux auxquels nous sommes quotidiennement confrontés : le salaire, l'entreprise, le capital, la concurrence, la qualité, la consommation, l'équité, le travail, la place de l'art et de la culture, la désertification rurale, etc. Pour autant, on ne lira pas ici la dernière théorie en vogue sur l'un ou l'autre de ces thèmes. Ce que nous propose Ardelaine, ce n'est pas d'affiner notre regard critique sur les incohérences du monde économique et social, c'est de trouver les voies pour se libérer de leurs influences.

## Du même auteur...

* *Chantier ouvert au public*, *Le Viel Audon, village coopératif*, Béatrice Barras, 2014, Editions REPAS. [Voir la fiche du livre](/Catalogue/chantier-ouvert/)
* *Une Cité aux mains fertiles*, *Quand les habitants transforment leur quartier*, Béatrice Barras, 2019, Editions REPAS. [Voir la fiche du livre](/catalogue/une-cite-aux-mains-fertiles/)
