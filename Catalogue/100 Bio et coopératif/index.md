---
Edition: Editions REPAS
DépotLégal: 2021
Autrice: Coopérative Cocebi
Titre: 100% Bio et Coopératif
SousTitre: Comment l’idée a germé de créer la première coop de producteurs bio
Préface: Préface de Claude Gruffat
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-17-4
Pages: 168 pages
Prix: 15
Etat: Disponible
Résumé: |
    Face aux dérives et dégâts de l'agriculture industrielle, des paysans cherchent des réponses qui préservent l'avenir. En Bourgogne, dans les années 1970-1980, ils passent leurs exploitations en agriculture biologique, se rencontrent et s'épaulent. « Pour beaucoup, les agriculteurs bios étaient des rigolos ! » se souvient l'un d'eux. Pionniers, ils créent en 1983 la première coopérative 100 % bio pour commercialiser leurs céréales.

    Depuis, la Cocebi, c'est son nom, a grandi et regroupe 250 adhérents. En tissant des liens avec d'autres coopératives, en travaillant avec le mouvement des coopératives de consommateurs, en défendant leur vision de l'agriculture et de la bio au sein des structures professionnelles et gouvernementales, ces hommes et ces femmes répondent aux grands enjeux environnementaux. Avec toujours cette exigence : 100 % bio et 100% coopératif !
    Une immersion passionnante dans une aventure humaine et paysanne pleine d'espoirs.

    [**Cocebi aujourd'hui**](https://www.cocebi.com/)
Tags:
- Coopérative
- Développement rural
- Agriculture bio
- Economie sociale et solidaire
SiteWeb: https://www.cocebi.com/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/100-bio-et-cooperatif-cocebi?_ga=2.210501397.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272174-100-bio-et-cooperatif-comment-l-idee-a-germe-de-creer-la-premiere-coop-de-producteurs-bio-cooperative-cocebi/
Couverture:
Bandeau: Nouveauté
AccueilOrdre:
Adresse: Sentier de la Fontaine, 89310 Nitry
Latitude: 47.67386
Longitude: 3.885176
Vidéos:
  https://www.youtube.com/watch?v=goT6d9xtgUM: La COCEBI, coopérative céréalière 100% Bio, fête ses 35 ans !
Couleurs:
  Titre: '#FCEC78'
  Fond: '#60985d'
  Texte: '#fdf1cc'
  PageTitres: '#11663d'
---

## Extraits

> On était des pionniers, se souvient Philippe Cabarat. On a créé une filière qui n’existait pas. Il en a fallu de l’obstination, de la conscience collective. S’engager, comprendre les objectifs communs et les différents chemins pour y parvenir. Nous avons beaucoup expérimenté et à l’époque nous étions parfois un peu "bulldozer", mais c’est en se trompant qu’on forge son expérience !
> -- page 32

> Cette embellie du bio réveille les appétits des coopératives conventionnelles qui veulent augmenter leur collecte bio. Résultat : les adhérents de la COCEBI, comme sans doute ceux des autres coopératives 100 % bio, sont démarchés par ces coopératives conventionnelles qui veulent « rentabiliser au plus vite leurs nouvelles installations dédiées au bio. » Fort heureusement, les adhérents restent fidèles à leur coopérative, et de nombreux agriculteurs qui ont nouvellement converti leur ferme viennent grossir les rangs de la COCEBI. Les atouts de la coopérative sont indéniables, à commencer par l’expérience agronomique accumulée par les coopérateurs depuis plus de 25 ans. À cela s’ajoutent la connaissance du marché bio et sans doute aussi la fidélité aux valeurs coopératives et éthiques.
> -- page 90
