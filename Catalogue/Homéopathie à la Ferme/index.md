---
Edition: Editions REPAS
DépotLégal: 2011
Réédition: 2016
Autrice: Association Homéopathie à la ferme
Titre: Homéopathie à la ferme
SousTitre: Des éleveurs racontent
Préface: Préface de Jocelyne Porcher
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-03-7
Pages: 252 pages
Prix: 16
Etat: Disponible
Résumé: |
    Agnès, Vincent, François, Yveline et les autres, sont éleveurs depuis de nombreuses années. Préoccupés par la question du bien être et de la santé de leurs animaux, ils ont la curiosité de s'intéresser aux médecines alternatives. La rencontre avec un vétérinaire homéopathe et une conseillère en élevages biologiques les conduit à se former, expérimenter, à échanger entre eux pour soigner autrement.

    Dans ce livre, ils témoignent de leurs réussites et de leurs tâtonnements ; mais bien au-delà d'une connaissance technique, ils nous parlent de patience, d'observation, d'entraide, de choix, de responsabilité... Un art de vivre avec les animaux qui interroge profondément notre vision de la santé.
    Ce livre est le fruit de ce cheminement collectif. Il se veut un outil à la disposition de toutes celles et ceux que la curiosité n'effraie pas.
    
    Le site internet de l'association [**ICI.**](https://homeoferme.dromy.co/)


Tags:
- Agriculture
- Alternatives
- Collectif
- Développement rural
SiteWeb: https://homeoferme.dromy.co/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/homeopathie-a-la-ferme?_ga=2.210442005.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272037-homeopathie-a-la-ferme-des-eleveurs-racontent-3e-edition-association-homeopathie-a-la-ferme/
Couverture:
Couleurs:
  Texte: '#E6EFE0'
  Titre: ' #F0AE6A'
  Fond: '#5b8740'
  PageTitres: '#d67b20'
Adresse: 26400 La Répara-Auriples
Latitude: 44.665043
Longitude: 4.997432
---

## Les auteurs

Ecrit collectivement sur plusieurs années, ce livre est à plusieurs voix. Chaque éleveur a pris la plume pour raconter son trajet et son exploitation, ensemble ils ont construit cet ouvrage où la parole de chacun a été respectée et rapportée.

## Extrait

> En alpage dans les années 1980, nous rencontrons Alain Boutonnet, alors vétérinaire homéopathe dans le Briançonnais, qui ne plaint ni son temps ni son énergie pour discuter avec les bergers des soins vétérinaires bien sûr, mais plus largement du rapport aux animaux, à la nature, à notre métier et à l'agriculture en général. Très vite, il nous donne envie d'en savoir plus sur ces remèdes si respectueux de l'animal et plus généralement de l'environnement. Leur prix, très inférieur aux médicaments classiques retient notre attention de jeunes éleveurs, débutant avec un petit budget. Nous venons en effet d'acheter quelques génisses qui montent avec nous en alpage : en tant que bergers salariés nous utilisons les antibiotiques fournis par nos patrons, mais sur nos bêtes, nous commençons à expérimenter l'homéopathie. L'alpage est une bonne école d'observation des animaux. Il nous faut maintenant apprendre à trier les symptômes pour le choix du remède et nous familiariser avec la matière médicale.
> Nous sommes plusieurs bergers ou jeunes installés dans les Hautes-Alpes et les départements voisins à rechercher des alternatives aux soins classiques. Alain nous propose d'organiser des journées de formation vétérinaire à la fois théorique et pratique, dans nos fermes.
> -- pages 46-47

## Le commentaire de Jocelyne Porcher

« *Homéo à la ferme* est un livre à offrir de toute urgence à tous ceux qui vivent avec un ou des animaux, à la ferme ou non. Non seulement parce qu'il est riche d'enseignements sur l'usage de l'homéopathie pour les animaux d'élevage mais aussi parce qu'il est bien plus que cela. Car l'homéopathie, ce n'est pas seulement une façon de soigner les animaux, c'est aussi, et peut-être surtout, une façon de vivre et de travailler avec eux. C'est ce que nous permettent de comprendre les éleveur(e)s qui ont participé à cet ouvrage avec le vétérinaire Alain Boutonnet.
Quelle belle idée d'écriture que ce travail collectif de réflexions, de partage d'expériences, de témoignages précis ou touchants autour du soin des animaux.
Et quelle merveilleuse manière de nous rappeler que les animaux d'élevage existent subjectivement, que chacun a sa personnalité, son caractère, que Amina et Savane, toutes deux chèvres Alpine, ne sont pas la même chèvre et que donc elles n'ont pas la même relation à la maladie et ne réclament pas le même remède. » - Jocelyne Porcher, préfacière, sociologue et zootechnicienne française

Pour aller plus loin : écoutez l'émission [Continent sciences](https://www.franceculture.fr/emissions/continent-sciences/lanimal-industriel) avec Jocelyne Porcher le 5/09/11 sur France Culture.


