---
Edition: Editions REPAS
DépotLégal: 2018
Réédition: 2019
Auteurs: Association Homéopathie à la ferme
Titre: Vade-mecum homéopathique de l’élevage en milieu pastoral
Collection: Collection Guide Pratique
ISBN : 978-2-919272-14-3
Pages: 160 pages
Prix: 15
Etat: Indisponible
Résumé: |
    Ce vade-mecum, guide pratique, est le résultat du travail collectif de bergers et d'un vétérinaire, qui souhaitent partager leur expérience et aider d'autres éleveurs et bergers à soigner par l'homéopathie. Le « regard homéopathique » porté aux bêtes conduit au plus profond de leur être, pour y découvrir leurs ressentis, leurs émotions et les comprendre dans leur totalité.

    L’apprentissage des remèdes, tous issus de substances naturelles qui nous entourent, est aussi passionnant que le message qu’ils transmettent. L’homéopathie est une formidable aventure humaine durant laquelle rencontres, échanges et partages se succèdent dans un esprit de solidarité et d’entraide. Comme le dit l'une des auteurs de ce livre : « D’éleveuse passive, soumise et ignorante, je suis devenue soigneuse réfléchie, responsable et libre. L’homéopathie nous ouvre tous les espaces du possible ! »
Tags:
- Agriculture
- Alternatives
- Soin
SiteWeb: https://homeoferme.dromy.co/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/vade-mecum-homeopathique-de-l-elevage-en-milieu-pastoral?_ga=2.206697491.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272143-vade-mecum-homeopathique-de-l-elevage-en-milieu-pastoral-association-homeopathie-a-la-ferme/
Couverture:
Couleurs:
    Fond: '#F28F55' #ou essayer #7c7839
    Titre: '#ffffff'
    Texte: '#ffffff'
    PageTitres: '#7d7841'
Adresse: 26400 La Répara-Auriples
Latitude: 44.665043
Longitude: 4.997432
---

Ce guide pratique à destination des bergers et des éleveurs-éleveuses vient compléter le témoignage du collectif d'éleveurs : [Homéopathie à la ferme](/catalogue/homeopathie-a-la-ferme/).


## Extraits

> **UTILISATION PRATIQUE DU VADE-MECUM**
>
> Vous êtes avec le troupeau en bergerie ou en montagne.
> Vous découvrez un animal malade (blessure, diarrhée, mammite, etc.).
> Prenez le temps d’observer l’animal et notez tous les symptômes.
> Ouvrez le vade-mecum et allez dans le chapitre concerné.
> Lisez chaque remède et choisissez celui qui correspond le mieux aux symptômes observés sur l’animal malade.
> Lorsque la dilution n’est pas précisée, il s’agit de remèdes curatifs à effet immédiat, les basses dilutions, 4, 5 et 7 ch sont utilisées le plus fréquemment.
> -- page 11

> **CONCLUSION**
> Résister dans ces conditions est une tâche ardue et pourtant nécessaire... Tout commence par de petits pas. À la montée en alpage, confectionner la trousse d’urgence est un premier pas. Témoigner qu’une thérapeutique économe et respectueuse du vivant existe, s’investir dans les échanges entre bergers et éleveurs, parler de ses échecs et de ses réussites, constituent un programme plein de promesse. Observer, écouter, toucher, ausculter, noter : tous ces gestes quotidiens ont une grande signification. C’est la consolidation du lien avec les animaux, tous les animaux, c’est "la reconnaissance d’une communauté de destin entre eux et nous" (Jocelyne Porcher), c’est être en phase avec la vie, c’est enfin témoigner que le chemin se fait en marchant.
> -- page 152

