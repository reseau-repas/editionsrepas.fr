---
Edition: Editions REPAS
DépotLégal: 2011
Autrice: Elisabeth Bost
Titre: Aux Entreprenants associés
SousTitre: La coopérative d’activités et d’emploi
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-01-3
Pages: 204 pages
Prix: 17
Etat: Disponible
Résumé: |
    Dans ce monde où le capitalisme ne cesse de détruire les relations humaines, est-il encore envisageable d’associer ces deux termes : travail et rêve ? C’est ce qu’affirme avec force Elisabeth Bost, à l’origine de la création d’une forme originale d’entrepreneuriat : les coopératives d’activités et d’emploi.

    Rassemblant plusieurs milliers d’entrepreneurs-salariés, ces jeunes structures de l’économie sociale et solidaire font chaque jour de nouveaux adeptes, des individus désireux de vivre de leur savoir-faire et animés de cette idée simple qu’ensemble on est plus fort que tout seul.

    S’appuyer sur la force du collectif pour développer son activité économique, recréer des solidarités sociales, c'est-à-dire faire passer l’épanouissement par la coopération, n'est-ce pas une voie pour rêver le travail ? Illustré de nombreux témoignages, cet ouvrage appréhende le fonctionnement pratique des coopératives d’activités et d’emploi comme le projet politique qui les sous-tend.
Tags:
- Coopérative
- Collectif
- Travail
- Economie sociale et solidaire
- Coopérative d'activités et d'emploi
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/aux-entreprenants-associes-elisabeth-bost?_ga=2.169402273.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272075-aux-entreprenants-associes-elisabeth-bost/
Couverture:
Couleurs:
  Fond: '#7b6939'
  Titre : '#E19857'
  Texte: '#efede3'
  PageTitres: '#E19857'
---

## L'autrice

Après s'être quelques temps frottée à la politique, **Elisabeth Bost** s'est prise de passion pour le développement économique et la création d'entreprise, jusqu'à poser la première pierre de cette nouvelle forme d'entrepreneuriat collectif : la coopérative d'activités et d'emploi.

Derrière la « douce rêveuse » se cache une femme de tête que rien ne fera dévier des valeurs coopératives qu'elle a chevillées au corps : depuis la création de Cap Services, la première CAE, à Lyon en 1995, jusqu'à celle de [Coopaname](https://www.coopaname.coop/) en 2004 à Paris, en passant par la délégation générale du réseau des CAE, Coopérer pour Entreprendre, entre 2002 et 2007. Elle anime aujourd'hui le programme AlterMed pour soutenir le développement d'entreprises partagées au Maghreb.


## Extrait

> C’est dans un contexte d’incitation massive des chômeurs à créer leur entreprise, d’échecs rencontrés et surtout de conscience qu’il faut non seulement disposer d’un savoir-faire mais aussi apprendre à vendre et à gérer, que sont apparues les CAE il y a 15 ans.
> Comment tester la viabilité de son projet sans prendre de risques inconsidérés ? Comment développer légalement et « grandeur nature » son activité sans prendre la responsabilité individuelle de son identité juridique ? En proposant un cadre juridique commun à l’ensemble des entrepreneurs qui, de manière mutualisée, vont bénéficier du numéro d’immatriculation nécessaire au développement d’une activité dans un cadre légal.
> Le créateur va ainsi pouvoir, de manière sereine, produire, démarcher sa clientèle, disposer du temps nécessaire (non quantifiable au préalable et différent pour chacun) à la mise en place et au  développement de son projet.
> En outre, tout en demeurant totalement autonome, il ne sera pas seul dans cette démarche mais bénéficiera d’un accompagnement individuel et collectif.
> -- pages 20-21

## L'histoire

Qu'est-ce qu'une coopérative d'activités et d'emploi (CAE) ? Comment ça fonctionne ? Comment cette nouvelle forme d'entreprendre est-elle née ? Cet ouvrage propose de partir à la découverte de ce mode original d'entrepreneuriat. Illustré de nombreux témoignages, il appréhende tout autant le fonctionnement pratique que le projet politique qui sous-tend la CAE.

Dans un premier temps, est explicité son fonctionnement au travers de témoignages d'entrepreneurs issus des CAE que l'auteur a connus : Cap Services, la première née en 1995 et de Coopaname, la plus importante CAE d'Ile de France. Dans une seconde partie c'est l'histoire de ces coopératives qui est racontée à travers l'itinéraire d'Elisabeth Bost : comment elles se sont co-construites, les difficultés rencontrées, les combats menés depuis quinze ans et ceux qui restent d'actualité, notamment en matière juridique.

La troisième partie est consacrée aux enjeux actuels du modèle. Elle prend appui sur l'expérience de Coopaname, la croissance rapide et la taille de cette coopérative l'obligeant à de nouveaux questionnements et la poussant sans cesse à l'innovation.

Pour cette nouvelle édition, une quatrième partie présente les évolutions les plus récentes concernant les CAE, en particulier leur entrée dans la loi en 2014. La démarche CAE est également illustrée par trois initiatives qui font un peu partie de la même famille et qui sont présentées dans cette nouvelle édition : Manucoop (La Manufacture coopérative), Bigre et SMart. Une façon de montrer que, sous des formes différentes, s'incarne un projet de société basé sur un fonctionnement économique qui repose sur des valeurs humaines et non sur le seul profit lié au capital.


## Le commentaire des éditeurs

L'histoire des CAE est instructive à plus d'un titre. Elle montre comment s'invente à un moment une nouvelle forme d'entreprenariat qui allie démarche individuelle et dimension collective, fonctionnement autonome et esprit coopératif, salariat et sociétariat. Un objet étrange dans le monde des entreprises qui répond pourtant à une demande sociale évidente. Comme l'explique Elisabeth Bost :

> Alors que la débâcle financière montre aujourd'hui les limites de l'entreprise capitaliste, les coopératives d'activités et d'emploi proposent une démarche collective et mutualisée d'entreprise. Pour autant, on ne saurait lier leur succès à la seule crise financière. Si elles attirent de plus en plus de candidats, c'est aussi parce qu'elles correspondent à des évolutions beaucoup plus profondes de notre société. Ce qui hier paraissait quelque peu ringard, voire désuet, revient aujourd'hui sur le devant de la scène. Le tiers secteur, les mutuelles, les coopératives ne sont plus considérées seulement comme des reliquats du passé mais connaissent un vigoureux regain d'actualité.

Les Coopératives d'Activités et d'emploi sont de celles-ci. Elles se co construisent depuis plus de quinze ans avec des personnes qui souhaitent créer leur emploi en développant une activité économique ; ceci avec le soutien des institutions, des collectivités et des fondations, pour certes apporter une réponse aux situations de chômage mais surtout en réinventant une relation au travail qui soit pour les personnes source d'épanouissement et d'émancipation. En combinant initiative personnelle et solidarité, elles correspondent aux deux attentes de l'individu contemporain : autonomie et recherche de liens sociaux.

L'intérêt de l'ouvrage est aussi de démontrer que l'innovation peut aussi être d'ordre institutionnel et juridique, même si celle-ci doit affronter des inerties et des obstacles particulièrement difficiles à faire bouger.

