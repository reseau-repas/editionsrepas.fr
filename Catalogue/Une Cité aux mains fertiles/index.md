---
Edition: Editions REPAS
DépotLégal: 2019
Autrice: Béatrice Barras
Titre: Une Cité aux mains fertiles
SousTitre: Quand les habitants transforment leur quartier
Préface: Préface de Claire Héber-Suffrin
Collection: Collection Pratiques Utopiques
ISBN : 978-2-919272-15-0
Pages: 164 pages
Récompenses: Prix du livre de l'économie sociale et solidaire 2020
Prix: 17
Etat: Disponible
Résumé: |
    Comment des habitants d’un quartier classé Zone urbaine sensible peuvent favoriser la participation de leurs voisins à l’amélioration de leur cadre de vie en les encourageant à agir ensemble, en faisant entendre leur parole, en relayant leurs besoins et leur souhaits auprès des institutions sans jamais se substituer à eux ?

    C’est la réponse victorieuse à cette question qu’apportent depuis plus de trente ans des femmes et des hommes qui ont ainsi transformé le quartier de Fontbarlettes, à Valence (Drôme). En commençant par rénover une cour et installer un atelier de tricotage, puis en cultivant des jardins au pied des immeubles.

    Plus d'infos sur les jardins partagés [**ICI.**](https://lematdrome.fr/)
Tags:
- Quartiers populaires
- Alternatives
- Collectif
- Economie sociale et solidaire
SiteWeb: https://lematdrome.fr/
CommandeWeb: https://www.helloasso.com/associations/association-repas/paiements/une-cite-aux-mains-fertiles-beatrice-barras?_ga=2.211554453.1091905683.1671446978-84470218.1590573003
TrouverLibrairie: https://www.placedeslibraires.fr/livre/9782919272150-une-cite-aux-mains-fertiles-quand-les-habitants-transforment-leur-quartier-beatrice-barras/
Couverture:
AccueilOrdre: 7
Adresse: 4 allée Séverine, 26000 Valence
Latitude: 44.930382
Longitude: 4.919851
Couleurs:
  Fond: '#7A081C'
  Texte: '#E9EEF2'
  Titre: '#E9EEF2'
  PageTitres: '#8a0820'
---


## L'autrice

**Béatrice Barras** est l'une des fondatrices de la Scop Ardelaine dans laquelle elle travaille toujours. Elle a également assumé diverses responsabilités auprès de différentes structures de l'économie sociale et solidaire, en particulier lorsqu'elle fut présidente du comité d'éthique de la NEF.

## Extraits

> **Des militants autogérés aux institutions du social**
>
> Ils ont créé un comité de quartier, un lieu de rencontre et d’expression pour les habitants qui a organisé bien des actions : un livret d’accueil des nouveaux venus, des cours d’alphabétisation, des sorties pour les enfants. Ce groupe était composé d’une alliance de militants chrétiens et communistes qui auront rapidement des divergences.
>
> Le comité faisait part des besoins du quartier à la municipalité : on est encore à cette époque dans la suite du mouvement de 1968, où l’ambiance était à l’autogestion et à la prise en mains par les intéressés de leur propre vie. Les réunions avec les élus de Valence étaient souvent houleuses. Ils n’avaient pas l’habitude de dialoguer avec des citoyens « porte-parole » qui venaient leur demander de faire ceci ou cela !
> -- page 24

> Le bâtiment K qu’ils habitent est conçu en carré avec une cour intérieure. Au centre, un ancien bac à sable est devenu le terrain de prédilection des chiens pour faire leurs besoins et divers déchets s’amoncèlent ou volent au vent. Un espace abandonné, un espace insalubre.
> Comment pourrait-il devenir agréable à regarder depuis les fenêtres des appartements et comment la cour du bâtiment pourrait-elle être utilisée par les résidents ?
> Il faudrait l’aménager. Quand on en parle à l’Office HLM, on a droit au récit de toutes les dernières destructions des espaces de jeux faits par la ville. Est-ce qu’une réhabilitation proposée et réalisée par les habitants eux-mêmes aurait plus d’avenir ? Serait-il possible de mobiliser les jeunes pour améliorer eux-mêmes leur espace de vie ?
> -- pages 45-46

## Le commentaire des éditeurs

*Une cité aux mains fertiles* vous invite à partager plus de 30 ans d’expérience dans le quartier de Fontbarlettes à Valence. L’histoire débute en 1986 avec l’installation d’un atelier de tricotage de la SCOP Ardelaine, rue Verdi. Il s’agit d’un établissement secondaire de la coopérative dont le siège et les activités principales développées autour de la transformation de la laine, se trouvent à Saint-Pierreville, un village ardéchois de la vallée de l’Eyrieux, à une heure de Valence.

Cet écrit n’est pas une analyse mais un récit, il relate des faits mis en œuvre par une multitude de personnes qui ont apporté au long cours ou ponctuellement, leur part petite ou grande. On y trouve de belles histoires, mais aussi des passages difficiles, voire douloureux, mais quelles que soient les situations ou le contexte, les principaux acteurs ont toujours su trouver dans les interstices des vérités établies, des défis, des alternatives, des solutions, des possibles, inlassables faiseurs de paix au service de la vie.


## Du même auteur...

* *Chantier ouvert au public*. *Le Viel Audon, village coopératif*, Béatrice Barras, 2014, Editions REPAS. [Voir la fiche du livre](/catalogue/chantier-ouvert/)
* *Moutons Rebelles. Ardelaine, la fibre développement local*, Béatrice Barras, 2014, Editions REPAS. [Voir la fiche du livre](/catalogue/moutons-rebelles/)

