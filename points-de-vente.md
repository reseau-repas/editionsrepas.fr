---
title: Points de vente — Où trouver les livres des Éditions REPAS
---

# Où trouver nos livres ?

Nos livres sont distribués partout en France, dans des librairies et lieux partenaires à retrouver **sur cette carte**. Cependant, cette liste n'est pas exhaustive : pour vérifier la disponibilité des ouvrages près de chez vous, rendez-vous sur le site [Place des libraires](https://www.placedeslibraires.fr/listeliv.php?form_recherche_avancee=ok&editeur=Repas&base=paper) ou commandez-les auprès de votre libraire.

- [Commander en ligne](https://www.helloasso.com/associations/association-repas/boutiques/editions-repas-commander-en-ligne){ .cta }
- [Commander par correspondance]({{ '/commander/' | url }}){ .cta }
- [Commander en librairie](https://www.placedeslibraires.fr/listeliv.php?form_recherche_avancee=ok&editeur=Repas&base=paper){ .cta }
{ .unstyled .inline }

<div id="carte" aria-hidden
  data-base-layer-url="{{ '/assets/map/metropole-version-simplifiee.geojson' | url }}"
  data-structures-url="{{ '/api/points-de-vente.json' | url }}"
  data-lieux-url="{{ '/api/lieux.json' | url }}"
  data-icones-id="vector-icons"
  data-marker-url="{{ '/assets/map/marker.png' | url }}"
  data-marker2-url="{{ '/assets/map/marker2.png' | url }}">
</div>

Si vous souhaitez recevoir nos ouvrages et devenir un lieu de vente permanent, merci de [nous contacter](mailto:repas@wanadoo.fr).

<table>
  <caption>Liste des points de vente avec leur site Web et adresse postale</caption>
  <thead>
    <tr>
      <th scope="col">Nom</th>
      <th scope="col">Adresse</th>
      <th scope="col">Ville</th>
      <th scope="col">Code postal</th>
    </tr>
  </thead>
  <tbody>
  {% for point in pointsDeVente.features %}
  <tr>
    <th scope="row">{% if point.properties.Url %}<a href="{{ point.properties.Url }}" target="_blank" rel="noopener">{{ point.properties.Nom }}</a>{% else %}{{ point.properties.Nom }}{% endif %}</th>
    <td>{{ point.properties.Adresse }}</td>
    <td>{{ point.properties.Ville }}</td>
    <td>{{ point.properties.CodePostal }}</td>
  </tr>
  {% endfor %}

  </tbody>
</table>

<link rel="stylesheet" href="{{ '/assets/map/maplibre-gl.css' | url }}">
<script type="module" src="{{ '/assets/map/controller.js' | url }}"></script>
