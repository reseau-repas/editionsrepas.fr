import { LngLatBounds, Popup, Map as MapLibreMap } from 'maplibre-gl'

function loadMarker ({ map, url, id }) {
  return new Promise((resolve, reject) => {
    map.loadImage(url, function (error, image) {
      if (error) {
        return reject(error)
      }

      resolve(map.addImage(id, image))
    })
  })
}

function displaySurroundingElements ({ popup, templates, radius }) {
  return ({ features, lngLat, target: map }) => {
    const [point] = features
    const { coordinates } = point.geometry

    const [layerId, template] = Object.entries(templates).find(([layerId, { checkFn }]) => checkFn(point))
    const html = template.render(point)

    popup
      .setLngLat(coordinates)
      .setHTML(html)
      .setMaxWidth('300px')
      .addTo(map)
  }
}

function setCursor (cursor) {
  return ({ target: map }) => {
    map.getCanvas().style.cursor = cursor
  }
}

function mapSetup (mapContainer) {
  const { baseLayerUrl, structuresUrl, lieuxUrl, iconesId, markerUrl, marker2Url } = mapContainer.dataset
  const { href: iconesUrl } = document.getElementById(iconesId)

  // Initialise la carte MapLibre avec des options
  const map = new MapLibreMap({
    // element HTML dans lequel la carte sera construite
    container: mapContainer,
    // niveau de zoom initial (1 = terre entière, 22 = ras du sol)
    zoom: 3,
    // niveau de zoom jusqu'où on peut reculer
    minZoom: 0,
    // niveau de zoom jusqu'où on peut plonger
    maxZoom: 10,
    // coordonnées GPS du centre de la carte (plus ou moins le milieu de la France métropolitaine)
    center: [2.209667, 46.232193],
    // carré de coordonnées GPS dans lequel on peut se déplacer
    // si on ne les précise pas, on peut aller se balader dans d'autres pays
    maxBounds: new LngLatBounds(
      [-17.391908446253353, 41.138099752838855],
      [24.376254930457293, 51.14611670016029],
    ),
    cooperativeGestures: {
      windowsHelpText: "Utilisez Ctrl + scroll pour zoomer sur la carte",
      macHelpText: "Utilisez ⌘ + scroll pour zoomer sur la carte",
      mobileHelpText: "Utilisez deux doigts pour zoomer sur la carte",
    },
    // quand on déplace la carte, actualise le "hash" avec les coordonnées du centre de la carte
    // exemple : #/-2,46,3.5
    hash: false,
    // langue des éléments textuels de la carte, s'il y en a
    locale: 'fr'
  })

  const style = {
    id: "carte",
    version: 8,
    sources: {
      // ajoute le découpage de la France métropolitaine comme source de données
      // c'est le fichier `js/metropole-version-simplifiee….geojson
      france: {
        type: "geojson",
        data: baseLayerUrl
      },
      // ajoute l'emplacement des structures REPAS comme source de données
      // c'est le fichier `api/structures.json` construit par `_data/structures.js` et publiées par `pages/api/structures.html`
      structures: {
        type: "geojson",
        data: structuresUrl
      },
      // ajoute l'emplacement des structures REPAS comme source de données
      // c'est le fichier `api/structures.json` construit par `_data/structures.js` et publiées par `pages/api/structures.html`
      lieux: {
        type: "geojson",
        data: lieuxUrl
      }
    },
    layers: [
      {
        // crée un calque d'affichage basé sur le découpage de la France
        // on remplit la forme vectorielle avec un couleur
        id: 'country-boundaries',
        type: 'fill',
        source: 'france',
        paint: {
          'fill-color': '#E5E5E5',
        }
      },
      {
        // crée un calque d'affichage des structures sous forme de marqueurs
        id: 'points',
        type: 'symbol',
        source: 'structures',
        layout: {
          'icon-allow-overlap': true,
          'icon-image': 'marker',
          'icon-size': 0.75
        }
      },
      {
        // crée un calque d'affichage des structures sous forme de marqueurs
        id: 'lieux',
        type: 'symbol',
        source: 'lieux',
        layout: {
          'icon-image': 'marker2',
          'icon-size': 0.75
        }
      }
    ]
  }

  Promise.all([
    loadMarker({ map, url: markerUrl, id: 'marker' }),
    loadMarker({ map, url: marker2Url, id: 'marker2' }),
  ]).then(() => map.setStyle(style))

  // création d'un objet "popup"
  // on fait des choses avec ce popup lors d'interactions sur la carte
  // @see https://maplibre.org/maplibre-gl-js-docs/api/markers/#popup
  const popup = new Popup({
    closeButton: true,
    closeOnClick: true,
    offset: {
      'top': [0, 0],
      'bottom': [0, 0]
    }
  })

  // quand on clique sur un point du calque `structures-points`, on affiche la popup
  const templates = {
    lieux: {
      checkFn (point) {
        return point.properties.Titre
      },
      render (point) {
        const { Titre, Adresse, url } = point.properties

        return `
          <h3 class="h4"><a href="${url}" rel="permalink">${Titre}</a></h3>
          <p class="type">
            <svg class="icon" aria-hidden="true"><use xlink:href="${iconesUrl}#book-mark-line"></use></svg>
            lieu de récit
          </p>
          <p class="address">${Adresse}</p>
        `
      }
    },
    points: {
      checkFn (point) {
        return point.properties.Nom
      },
      render (point) {
        const { Nom, Adresse, CodePostal, Ville, Url } = point.properties

        return `
          <h3 class="h4">${Nom}</h3>
          <p class="type">
            <svg class="icon" aria-hidden="true"><use xlink:href="/assets/icons/index.svg#store-2-line"></use></svg>
            point de vente
          </p>
          <p class="address">${Adresse}, ${CodePostal} ${Ville}</p>
          ${Url && `<p class="website">
            <a href="${Url}" target="_blank" rel="noopener noreferrer">${Url}</a>
          </p>`}`
      }
    }
  }

  map.on('click', 'points', displaySurroundingElements({ popup, templates, radius: 10 }))
  map.on('click', 'lieux', displaySurroundingElements({ popup, templates, radius: 10 }))

  // on change le curseur de la souris lorsqu'on survole un marquer de structure
  map.on('mouseenter', 'points', setCursor('pointer'))
  map.on('mouseleave', 'points', setCursor(''))
  map.on('mouseenter', 'lieux', setCursor('pointer'))
  map.on('mouseleave', 'lieux', setCursor(''))
}


/**
 * Setup the map on page load, if we can see a `<div id="structures-carte">` element
 */
document.addEventListener('DOMContentLoaded', () => {
  const mapContainer = document.querySelector('#carte')

  if (mapContainer) {
    mapSetup(mapContainer)
  }
})
