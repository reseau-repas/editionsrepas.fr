const { parse } = require('csv-parse')
const { readFile } = require('fs/promises')
const { join, dirname } = require('path')

module.exports = async function(configData) {
  const { decode } = await import('windows-1252')
  const csvFilename = join(configData.eleventy.env.root, 'points-de-vente.csv')
  const records = []
  const csvContent = await readFile(csvFilename)
  const parser = parse(decode(csvContent), {
    columns: ['Id', 'Nom', /*'Email', */'Url', 'Adresse', 'CodePostal', 'Ville', 'latitude', 'longitude', ...Array(16)],
    delimiter: ';',
    from_line: 2,
    trim: true,
    cast: true,
  })

  for await(const record of parser) {
    if (record.Nom) {
      records.push(record)
    }
  }

  const features = records
    .sort((a, b) => a.CodePostal - b.CodePostal)
    .map(record => {
      const coordinates = [
        parseFloat(record.longitude),
        parseFloat(record.latitude)
      ]

      const properties = Object.entries(record).reduce((obj, [key, value]) => {
        if (/^result_/.test(key) === false) {
          obj[key] = value
        }

        return obj
      }, {})

      return {
        type: 'Feature',
        properties,
        geometry: {
          type: 'Point',
          coordinates
        }
      }
    })

  return {
    type: 'FeatureCollection',
    features
  }
}
