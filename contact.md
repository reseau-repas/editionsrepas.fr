---
title: Nous contacter
htmlClass: taille-étroite
---

# Nous contacter

**Editions REPAS**

4, allée Séverine    
26 000 VALENCE    
Tel : 04 75 42 67 45     
E-mail : repas[at]wanadoo.fr

Pour connaître nos tarifs d'expédition et conditions de vente à l'usage de libraires et professionnels, merci de lire notre page [Conditions de vente](/conditions-de-vente/) ou [nous contacter](mailto:repas@wanadoo.fr).

Pour prendre connaissance de nos Conditions Générales de Vente, merci de vous rendre sur la [page Mentions Légales](/mentions-legales/).

## Inscription à la lettre des Editions REPAS

Vous souhaitez être informé des dernières nouveautés des Editions REPAS ?

Vous recevrez quatre fois par an notre lettre d'information avec toutes les actualités des Editions REPAS, les prochains livres, nos projets et nos participations sur les foires, salons et festivals dans toute la France.

<iframe width="540" height="305" src="https://089ff32e.sibforms.com/serve/MUIFAPVsJ41-YzJPPqjS64tgJ6tMKyYr4-ylYiTXIwLJPQkwC7uxRGFlQbSU2KNch6hhy1x8IkwtwCpPGyjGjyb-2epmV7fZSPLUNikI6nGwQP-wrt0sGGCOf4H7gGfqw6ssEOM1iYTliaFf4LljEyJGjn2CeG_M2BK_zRjHLT4A8Nq1brYVaCBwoE380dDyzCmJjqzLDhOFXA58" frameborder="0" scrolling="auto" allowfullscreen style="display: block;margin-left: auto;margin-right: auto;max-width: 100%;"></iframe>

![insert image editions-repas.png](/assets/editions-repas.png)
