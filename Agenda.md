---
title: Agenda de nos événements
header:
htmlClass: taille-étroite
---

# Agenda

<iframe width="100%" height="500" src="https://apps.compagnonnage-repas.org/nextcloud/apps/calendar/embed/yPjamM6xDJz3kNty/listMonth/now" loading="eager"></iframe>

## Evénements à venir

**Fête du livre de Chalencon** : 30 juillet 2023, Chalencon (Ardèche)

## Evénements passés

**Assemblée Générale annuelle de l'association REPAS** : 20 juin 2023, Valence (Drôme)

**Rencontres du réseau REPAS 2023** : 18 et 19 mai 2023, Ecocentre du Trégor (Bretagne)

**Festival Comm'Un Printemps 23** : 29 avril 2023, Tournon-sur-Rhône (Drôme)

**Chantier d'écriture à la ressourcerie Court-Circuit** : 19 et 20 janvier 2023, Felletin (Creuse)

**[Salon Tatou Juste](https://www.tatoujuste.org/)** : 26 et 27 novembre 2022, Saint-Etienne (Loire)

**Chantier d'écriture à la Ferme de la Batailleuse** : du 18 au 20 novembre 2022, La Batailleuse (Haut-Doubs)

**[Lancement du compagnonnage 2022](https://compagnonnage-repas.org/pdf/2022_01_06_intercalaire2022_NB.pdf)** : du 30 septembre au 9 octobre, Lugan (Aveyron)

**Université Coopérative Portative** : 23 septembre 2022, Rennes (Ille-et-Vilaine)

**[Bigre! Rencontre](https://www.bigre.coop/)** : 22 au 26 août 2022, Sète (Hérault)

**40ème anniversaire de la SCOP Ardelaine** : 10-11 juillet 2022, St-Pierreville (Ardèche)

**[Dialogues en Humanités](https://dialoguesenhumanite.org/)** : du 1 au 3 Juillet, Lyon (Rhône)

**[Rencontres Inter-professionnelles du Livre](https://auvergnerhonealpes-livre-lecture.org/articles/retour-sur-les-rencontres-interprofessionnelles-du-livre-5)** : 23 juin 2022, Lyon (Rhône)

**Assemblée générale de l'association REPAS** : 14 Juin 2022, Valence (Drôme)

**[Festival des Pas-Sages](https://les-pas-sages.org/festival-les-pas-sages-au-hameau-des-buis/)** : 11 Juin 2022, Hameau des Buis (Ardèche)

**Salon Primevère** : 25 au 27 février 2022, Lyon (Rhône)

**Causerie : le récit, un outil pour diffuser les alternatives** : 11 Janvier 2022, café associatif Le Cause Toujours, Valence (Drôme)
